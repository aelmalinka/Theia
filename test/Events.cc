/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Vulkan/Events.hh"

using namespace std;
using namespace testing;
using namespace Theia::Vulkan::Events;
using Coeus::Severity;

namespace {
	TEST(EventsDebugTests, LogSeverityError) {
		{
			auto d = Debug(VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT, VK_NULL_HANDLE, VK_NULL_HANDLE);

			EXPECT_EQ(d.LogSeverity(), Severity::Error);
		}
		{
			auto d = Debug(VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT, VK_NULL_HANDLE, VK_NULL_HANDLE);

			EXPECT_NE(d.LogSeverity(), Severity::Error);
		}
	}
	TEST(EventsDebugTests, LogSeverityWarning) {
		{
			auto d = Debug(VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT, VK_NULL_HANDLE, VK_NULL_HANDLE);

			EXPECT_EQ(d.LogSeverity(), Severity::Warning);
		}
		{
			auto d = Debug(VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT, VK_NULL_HANDLE, VK_NULL_HANDLE);

			EXPECT_NE(d.LogSeverity(), Severity::Warning);
		}
	}
	TEST(EventsDebugTests, LogSeverityInfo) {
		{
			auto d = Debug(VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT, VK_NULL_HANDLE, VK_NULL_HANDLE);

			EXPECT_EQ(d.LogSeverity(), Severity::Info);
		}
		{
			auto d = Debug(VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT, VK_NULL_HANDLE, VK_NULL_HANDLE);

			EXPECT_NE(d.LogSeverity(), Severity::Info);
		}
	}
	TEST(EventsDebugTests, LogSeverityDebug) {
		{
			auto d = Debug(VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT, VK_NULL_HANDLE, VK_NULL_HANDLE);

			EXPECT_EQ(d.LogSeverity(), Severity::Debug);
		}
		{
			auto d = Debug(VkDebugUtilsMessageSeverityFlagBitsEXT(0), VK_NULL_HANDLE, VK_NULL_HANDLE);

			EXPECT_NE(d.LogSeverity(), Severity::Debug);
		}
	}

	TEST(EventsDebugTests, TypeNameUnknown) {
		auto d = Debug(VkDebugUtilsMessageSeverityFlagBitsEXT(0), VK_NULL_HANDLE, VK_NULL_HANDLE);
		EXPECT_EQ(d.TypeName(), "Unknown"s);
	}
	TEST(EventsDebugTests, TypeNameGeneral) {
		auto d = Debug(VkDebugUtilsMessageSeverityFlagBitsEXT(0), VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT, VK_NULL_HANDLE);
		EXPECT_EQ(d.TypeName(), "General"s);
	}
	TEST(EventsDebugTests, TypeNameValidation) {
		auto d = Debug(VkDebugUtilsMessageSeverityFlagBitsEXT(0), VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT, VK_NULL_HANDLE);
		EXPECT_EQ(d.TypeName(), "Validation"s);
	}
	TEST(EventsDebugTests, TypeNamePerformance) {
		auto d = Debug(VkDebugUtilsMessageSeverityFlagBitsEXT(0), VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT, VK_NULL_HANDLE);
		EXPECT_EQ(d.TypeName(), "Performance"s);
	}
}
