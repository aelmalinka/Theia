/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Vulkan/QueueFamily.hh"
#include <set>
#include <type_traits>

using namespace std;
using namespace testing;
using namespace Theia::Vulkan;

namespace {
	TEST(QueueFamilyTests, Get) {
		auto x = 0u;
		auto inst = make_shared<Instance>("Theia QueueFamily Tests"s, ApplicationInfo::Version);
		// 2020-04-07 AMR NOTE: select last device (shouldn't matter which one)
		auto phys = PhysicalDevice::select(inst, [&x](auto const &) { return ++x; });

		auto idxs = set<
			remove_cvref_t<decltype(phys->QueueFamilies().front().Index())>
		>{};
		for(auto &qf : phys->QueueFamilies()) {
			EXPECT_EQ(idxs.find(qf.Index()), idxs.end());
			// 2020-04-08 AMR TODO: is it safe to assume that there are no zero len queue families?
			EXPECT_NE(qf.Properties().queueCount, 0u);
			// 2020-04-08 AMR TODO: test canPresent?
			idxs.insert(qf.Index());
		}
	}
}
