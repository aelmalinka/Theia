/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../src/Vulkan/PhysicalDevice.hh"
#include <limits>

using namespace std;
using namespace testing;
using namespace Theia::Vulkan;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(PhysicalDeviceTests, Get) {
		TEST_BEGIN
		auto x = numeric_limits<int>::max();
		auto inst = make_shared<Instance>("Theia PhysDev Tests"s, ApplicationInfo::Version);
		// 2020-04-07 AMR NOTE: select first device
		auto phys = PhysicalDevice::select(inst, [&x](auto const &) {
			return x--;
		});

		EXPECT_FALSE(phys->Handle() == VK_NULL_HANDLE);
		EXPECT_EQ(phys->getInstance(), inst);
		EXPECT_NE(phys->Properties().apiVersion, 0u);
		// 2020-04-07 AMR TODO: is it safe to assume all devices have at least 1 queue family?
		EXPECT_NE(phys->QueueFamilies().size(), 0u);
		// 2020-04-07 AMR NOTE: Features are all boolean flags that are dependent on the device; just make sure it'll pull in
		phys->Features();
		TEST_END
	}
}
