/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Vulkan/detail/extlay.hh"

using namespace std;
using namespace testing;
using namespace Theia::Vulkan;

namespace {
	// 2020-04-02 AMR NOTE: testing of detail:: is not being done as the interface is very likely to change and should be validated in public api
	// 2020-04-02 AMR TODO: is this a good thing or a bad thing?
	// 2020-04-02 AMR NOTE: requires that VK_EXT_DEBUG_UTILS_EXTENSION_NAME is provided and valid (likely)
	TEST(Extension, Name) {
		auto ext = Extension(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

		EXPECT_EQ(string(ext.Name()), string(VK_EXT_DEBUG_UTILS_EXTENSION_NAME));
		EXPECT_TRUE(ext.isFound());
		EXPECT_FALSE(ext.isRequired());
	}

	// 2020-04-02 AMR NOTE: requires that VK_LAYER_LUNARG_standard_validation is provided and valid
	TEST(Layer, Name) {
		auto ext = Layer("VK_LAYER_LUNARG_standard_validation");

		EXPECT_EQ(string(ext.Name()), string("VK_LAYER_LUNARG_standard_validation"));
		EXPECT_TRUE(ext.isFound());
		EXPECT_FALSE(ext.isRequired());
	}

	// 2020-04-02 AMR NOTE: requires that 'asdf' is not a valid extensions (likely)
	TEST(Extension, NotFound) {
		EXPECT_THROW(Extension("asdf", true), NotFound);
	}

	// 2020-04-02 AMR NOTE: requires that 'asdf' is not a valid layer (likely)
	TEST(Layer, NotFound) {
		EXPECT_THROW(Layer("asdf", true), NotFound);
	}
}
