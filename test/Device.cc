/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Vulkan/Device.hh"

using namespace std;
using namespace testing;
using namespace Theia::Vulkan;

namespace {
	TEST(DeviceTests, Create) {
		auto x = 0u;
		auto q = vector<QueueInfo>{};
		auto inst = make_shared<Instance>("Theia Device Tests"s, ApplicationInfo::Version);
		auto phys = PhysicalDevice::select(inst, [&x, &q](auto const &d) {
			for(auto i = d->QueueFamilies().begin(); i != d->QueueFamilies().end(); i++)
				q.emplace_back(i);

			return ++x;
		});

		EXPECT_NE(q.size(), 0u);
		auto dev = make_shared<Device>(phys, q);

		EXPECT_FALSE(dev->Handle() == VK_NULL_HANDLE);
		EXPECT_EQ(dev->getPhysical(), phys);
	}
}
