/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../src/Vulkan/Debug.hh"

using namespace std;
using namespace Theia::Vulkan;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(DebugTests, Create) {
		TEST_BEGIN
		Debug debug;

		EXPECT_EQ(debug.Info().sType, VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT);
		EXPECT_EQ(debug.Info().flags, 0u);
		EXPECT_EQ(debug.Info().pNext, nullptr);
		TEST_END
	}
}
