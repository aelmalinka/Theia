/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Vulkan/Instance.hh"

using namespace std;
using namespace testing;
using namespace Theia::Vulkan;

namespace {
	TEST(InstanceTests, CreateDefault) {
		Instance inst;

		EXPECT_NE(&inst.Handle(), nullptr);
	}

	TEST(InstanceTests, CreateEmpty) {
		Instance inst{
			"Test Application"s,
			ApplicationInfo::Version
		};

		// 2020-04-06 AMR NOTE: EXPECT_NE(inst.Handle(), VK_NULL_HANDLE); fails to compile
		EXPECT_FALSE(inst.Handle() == VK_NULL_HANDLE);
		EXPECT_EQ(string(inst.Info().Handle().pApplicationName), "Test Application"s);
	}
}
