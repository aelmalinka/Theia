/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Vulkan/QueueInfo.hh"

using namespace std;
using namespace testing;
using namespace Theia::Vulkan;

namespace {
	TEST(QueueInfo, Create) {
		auto x = 0u;
		auto inst = make_shared<Instance>("Theia Device Tests"s, ApplicationInfo::Version);
		auto phys = PhysicalDevice::select(inst, [&x](auto const &) { return ++x; });

		auto qfs = vector<decltype(phys->QueueFamilies().begin())>{};
		{
			auto i = phys->QueueFamilies().begin();
			while(i != phys->QueueFamilies().end()) {
				qfs.push_back(i++);
			}
		}

		auto qis = vector<QueueInfo>{};
		for(auto const &qf : qfs) {
			qis.emplace_back(qf);
		}

		for(auto const &qi : qis) {
			auto qf = qi.Family();

			EXPECT_EQ(qi.Info().sType, VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO);
			EXPECT_EQ(qi.Info().pNext, nullptr);	// 2020-04-12 AMR TODO: global priority?
			EXPECT_EQ(qi.Info().flags, 0u);			// 2020-04-12 AMR TODO: protected memory
			EXPECT_EQ(qi.Info().queueFamilyIndex, qf->Index());
			EXPECT_LE(qi.Info().queueCount, qf->Properties().queueCount);
			EXPECT_GT(qi.Info().queueCount, 0u);
			for(auto i = 0u; i < qi.Info().queueCount; i++) {
				EXPECT_GE(qi.Info().pQueuePriorities[i], 0.0f);
				EXPECT_LE(qi.Info().pQueuePriorities[i], 1.0f);
			}
		}

		auto infos = QueueInfo::Convert(qis);
		for(auto const &info : infos) {
			// 2020-04-12 AMR TODO: queue family

			EXPECT_EQ(info.sType, VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO);
			EXPECT_EQ(info.pNext, nullptr);	// 2020-04-12 AMR TODO: global priority?
			EXPECT_EQ(info.flags, 0u);		// 2020-04-12 AMR TODO: protected memory
			EXPECT_GT(info.queueCount, 0u);
			for(auto i = 0u; i < info.queueCount; i++) {
				EXPECT_GE(info.pQueuePriorities[i], 0.0f);
				EXPECT_LE(info.pQueuePriorities[i], 1.0f);
			}
		}
	}
}
