/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_EVENT_INC
#	define THEIA_EVENT_INC

#	include <Coeus/Event.hh>

	namespace Theia
	{
		class Event : 
			public Coeus::Event
		{
			public:
				Event() = default;
				virtual ~Event() = default;
		};
	}

#endif
