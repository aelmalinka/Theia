/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_EXCEPTION_INC
#	define THEIA_EXCEPTION_INC

#	include <Coeus/Exception.hh>
#	include <Coeus/Log.hh>

	namespace Theia
	{
		using Coeus::Log::Severity;

		COEUS_EXCEPTION(Exception, "Theia Exception", Coeus::Exception);

		extern Coeus::Log::Source Log;
	}

#endif
