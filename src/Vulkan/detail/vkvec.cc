/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "../Exception.hh"
#include "vkvec.hh"

#include "../PhysicalDevice.hh"

using namespace std;

namespace Theia { inline namespace Vulkan { namespace detail {
	template<>
	template<>
	auto vkvec<VkExtensionProperties>::_count() const -> size_type {
		auto ret = uint32_t(0);
		EVK_SUCCESS_OR_THROW(vkEnumerateInstanceExtensionProperties(nullptr, &ret, nullptr), "Failed to get Extensions count"s);
		return ret;
	}

	template<>
	template<>
	auto vkvec<VkExtensionProperties>::_fill() -> void {
		auto cnt = uint32_t(size());
		EVK_SUCCESS_OR_THROW(vkEnumerateInstanceExtensionProperties(nullptr, &cnt, data()), "Failed to get Extensions"s);
	}

	template<>
	template<>
	auto vkvec<VkLayerProperties>::_count() const -> size_type {
		auto ret = uint32_t(0);
		EVK_SUCCESS_OR_THROW(vkEnumerateInstanceLayerProperties(&ret, nullptr), "Failed to get Layers count"s);
		return ret;
	}

	template<>
	template<>
	auto vkvec<VkLayerProperties>::_fill() -> void {
		auto cnt = uint32_t(size());
		EVK_SUCCESS_OR_THROW(vkEnumerateInstanceLayerProperties(&cnt, data()), "Failed to get Layers"s);
	}

	template<>
	template<>
	auto vkvec<VkPhysicalDevice>::_count(shared_ptr<Instance> const &instance) const -> size_type {
		auto ret = uint32_t(0);
		EVK_SUCCESS_OR_THROW(vkEnumeratePhysicalDevices(instance->Handle(), &ret, nullptr), "Failed to get PhysicalDevice count"s);
		return ret;
	}

	template<>
	template<>
	auto vkvec<VkPhysicalDevice>::_fill(shared_ptr<Instance> const &instance) -> void {
		auto cnt = uint32_t(size());
		EVK_SUCCESS_OR_THROW(vkEnumeratePhysicalDevices(instance->Handle(), &cnt, data()), "Failed to get PhysicalDevice"s);
	}

	template<>
	template<>
	auto vkvec<VkQueueFamilyProperties>::_count(shared_ptr<PhysicalDevice> const &device) const -> size_type {
		auto ret = uint32_t(0);
		vkGetPhysicalDeviceQueueFamilyProperties(device->Handle(), &ret, nullptr);
		return ret;
	}

	template<>
	template<>
	auto vkvec<VkQueueFamilyProperties>::_fill(shared_ptr<PhysicalDevice> const &device) -> void {
		auto cnt = uint32_t(size());
		vkGetPhysicalDeviceQueueFamilyProperties(device->Handle(), &cnt, data());
	}
}}}
