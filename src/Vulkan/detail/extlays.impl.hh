/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DETAIL_EXTLAYS_IMPL
#	define THEIA_VULKAN_DETAIL_EXTLAYS_IMPL

	namespace Theia
	{
		inline namespace Vulkan
		{
			namespace detail
			{
				template<typename T>
				auto extlays<T>::find(std::string const &name) const -> iterator {
					return _data.find(name);
				}

				template<typename T>
				auto extlays<T>::end() const -> iterator {
					return _data.end();
				}
			}
		}
	}

#endif
