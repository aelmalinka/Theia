/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DETAIL_VKVEC_IMPL
#	define THEIA_VULKAN_DETAIL_VKVEC_IMPL

	namespace Theia
	{
		inline namespace Vulkan
		{
			namespace detail
			{
				template<typename T>
				template<typename ...Args>
				vkvec<T>::vkvec(Args && ...args) :
					base()
				{
					this->resize(this->_count(std::forward<Args>(args)...));
					this->_fill(std::forward<Args>(args)...);
				}
			}
		}
	}

#endif
