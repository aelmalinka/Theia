/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DETAIL_EXTLAY_IMPL
#	define THEIA_VULKAN_DETAIL_EXTLAY_IMPL

	namespace Theia
	{
		inline namespace Vulkan
		{
			namespace detail
			{
				template<typename T>
				extlay<T>::extlay(std::string &&name, bool required) :
					base(),
					_iter(this->shared()->find(name)),
					_required(required)
				{
					if(_required && !this->isFound())
						COEUS_THROW(NotFound(Type) << ::Theia::Vulkan::Name(name));
				}

				template<typename T>
				auto extlay<T>::isFound() const -> bool {
					return _iter != this->shared()->end();
				}

				template<typename T>
				auto extlay<T>::isRequired() const -> bool {
					return _required;
				}

				template<typename T>
				auto extlay<T>::Name() const -> char const * {
					return _iter->first.c_str();
				}

				template<typename T>
				auto extlay<T>::Properties() const -> T const & {
					return _iter->second;
				}
			}
		}
	}

#endif
