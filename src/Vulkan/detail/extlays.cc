/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "extlays.hh"
#include "vkvec.hh"
#include <vector>

using namespace std;

using Coeus::Log::Severity;
using Theia::Log;

namespace Theia { inline namespace Vulkan { namespace detail {
	template<>
	extlays<VkExtensionProperties>::extlays() :
		_data()
	{
		auto v = vkvec<VkExtensionProperties>();
		for(auto &i : v) {
			COEUS_LOG(Log, Severity::Debug) << i.extensionName << " extension found";
			_data.emplace(i.extensionName, i);
		}
	}

	template<>
	extlays<VkLayerProperties>::extlays() :
		_data()
	{
		auto v = vkvec<VkLayerProperties>();
		for(auto &i : v) {
			COEUS_LOG(Log, Severity::Debug) << i.layerName << " layer found";
			_data.emplace(i.layerName, i);
		}
	}
}}}
