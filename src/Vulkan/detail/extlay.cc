/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "extlay.hh"

using namespace std;

namespace Theia { inline namespace Vulkan { namespace detail {
	template<>
	string const extlay<VkExtensionProperties>::Type("Extension"s);

	template<>
	string const extlay<VkLayerProperties>::Type("Layer"s);
}}}
