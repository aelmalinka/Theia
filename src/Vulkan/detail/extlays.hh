/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DETAIL_EXTLAYS_INC
#	define THEIA_VULKAN_DETAIL_EXTLAYS_INC

#	include <map>
#	include "../Exception.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			namespace detail
			{
				template<typename T>
				class extlays {
					private:
						using map = std::map<std::string, T>;
					public:
						using iterator = map::const_iterator;
					private:
						map _data;
					public:
						extlays();
						auto find(std::string const &) const -> iterator;
						auto end() const -> iterator;
				};
			}
		}
	}

#	include "extlays.impl.hh"

#endif
