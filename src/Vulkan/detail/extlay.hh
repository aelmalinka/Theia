/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DETAIL_EXTLAY_INC
#	define THEIA_VULKAN_DETAIL_EXTLAY_INC

#	include <map>
#	include <Coeus/SharedData.hh>
#	include "../Exception.hh"
#	include "extlays.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			namespace detail
			{
				template<typename T>
				class extlay :
					private Coeus::SharedData<extlays<T>>
				{
					private:
						using base = Coeus::SharedData<extlays<T>>;
						static std::string const Type;
					private:
						extlays<T>::iterator _iter;
						bool _required;
					public:
						extlay(std::string &&, bool = false);
						auto isFound() const -> bool;
						auto isRequired() const -> bool;
						auto Name() const -> char const *;
						auto Properties() const -> T const &;
				};
			}

			using Extension = detail::extlay<VkExtensionProperties>;
			using Layer = detail::extlay<VkLayerProperties>;
		}
	}

#	include "extlay.impl.hh"

#endif
