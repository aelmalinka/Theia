/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DETAIL_VKVEC_INC
#	define THEIA_VULKAN_DETAIL_VKVEC_INC

#	include <vector>

	namespace Theia
	{
		inline namespace Vulkan
		{
			namespace detail
			{
				template<typename T>
				class vkvec :
					public std::vector<T>
				{
					private:
						using base = std::vector<T>;
						using size_type = base::size_type;
					public:
						template<typename ...Args>
						vkvec(Args && ...);
					private:
						template<typename ...Args>
						auto _count(Args && ...) const -> size_type;
						template<typename ...Args>
						auto _fill(Args && ...) -> void;
				};
			}
		}
	}

#	include "vkvec.impl.hh"

#endif
