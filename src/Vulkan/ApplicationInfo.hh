/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_APPLICATION_INFO_INC
#	define THEIA_VULKAN_APPLICATION_INFO_INC

#	include <vector>
#	include "detail/extlay.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			class ApplicationInfo
			{
				private:
					VkApplicationInfo _handle;
					std::string _name;
					std::vector<Extension> _ext;
					std::vector<Layer> _lay;
					std::vector<char const *> _extensions;
					std::vector<char const *> _layers;
				public:
					ApplicationInfo(std::string &&, std::uint32_t const);
					auto Handle() const -> decltype(_handle) const &;
					auto Extensions() const -> decltype(_extensions) const &;
					auto Layers() const -> decltype(_layers) const &;
					auto Add(Extension &&) -> decltype(*this) &;
					auto Add(Layer &&) -> decltype(*this) &;
					static constexpr auto Version = VK_MAKE_VERSION(0, 0, 1);
			};
		}
	}

#endif
