/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "QueueFamily.hh"
#include "detail/vkvec.hh"

using namespace Theia::Vulkan;
using namespace std;

QueueFamily::QueueFamily(
	decltype(_device) const &device,
	decltype(_index) &&index,
	decltype(_prop) &&prop
) :
	_device(device),
	_index(index),
	_prop(prop)
{}

QueueFamily::QueueFamily(QueueFamily &&) = default;

auto QueueFamily::getPhysicalDevice() const -> decltype(_device) const & {
	return _device;
}

auto QueueFamily::Index() const -> decltype(_index) const & {
	return _index;
}

auto QueueFamily::Properties() const -> decltype(_prop) const & {
	return _prop;
}

auto QueueFamily::canPresent(shared_ptr<Surface> const &surf) const -> bool {
	VkBool32 v = false;
	EVK_SUCCESS_OR_THROW(
		vkGetPhysicalDeviceSurfaceSupportKHR(getPhysicalDevice()->Handle(), Index(), surf->Handle(), &v),
		"Failed to get Surface Support"s
	);
	return v;
}

auto QueueFamily::get(shared_ptr<PhysicalDevice> const &device) -> vector<QueueFamily> {
	auto v = detail::vkvec<VkQueueFamilyProperties>(device);
	auto r = vector<QueueFamily>{};
	auto idx = 0u;

	for(auto &qf : v) {
		r.emplace_back(move(QueueFamily(device, idx++, move(qf))));
	}

	return r;
}
