/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "Device.hh"

using namespace Theia::Vulkan;
using namespace std;

Device::Device(
	decltype(_phys) const &physical,
	vector<QueueInfo> const &queues,
	vector<char const *> const &ext
) :
	_phys(physical),
	_handle(VK_NULL_HANDLE)
{
	auto q = QueueInfo::Convert(queues);

#	ifdef DEBUG
		// 2020-04-12 AMR TODO: allow user to set some of these
		auto features = VkPhysicalDeviceFeatures{};
		// 2020-04-12 AMR NOTE: required for GPU_ASSISTED validation
		features.vertexPipelineStoresAndAtomics = VK_TRUE;
		features.fragmentStoresAndAtomics = VK_TRUE;
#	endif

	auto create = VkDeviceCreateInfo{
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.queueCreateInfoCount = static_cast<uint32_t>(q.size()),
		.pQueueCreateInfos = q.data(),
		.enabledLayerCount = static_cast<uint32_t>(_phys->getInstance()->Info().Layers().size()),
		.ppEnabledLayerNames = _phys->getInstance()->Info().Layers().data(),
		.enabledExtensionCount = static_cast<uint32_t>(ext.size()),
		.ppEnabledExtensionNames = ext.data(),
#		ifdef DEBUG
			.pEnabledFeatures = &features
#		else
			.pEnabledFeatures = nullptr
#		endif
	};

	EVK_SUCCESS_OR_THROW(
		vkCreateDevice(_phys->Handle(), &create, nullptr, &_handle),
		"Failed to create Device"s
	);
}

Device::~Device() {
	wait();
	vkDestroyDevice(_handle, nullptr);
}

auto Device::Handle() const -> decltype(_handle) const & {
	return _handle;
}

auto Device::getPhysical() const -> decltype(_phys) const & {
	return _phys;
}

auto Device::wait() -> void {
	EVK_SUCCESS_OR_THROW(
		vkDeviceWaitIdle(_handle),
		"Failed to wait for device to idle"s
	);
}
