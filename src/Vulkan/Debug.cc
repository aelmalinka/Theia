/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>'
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Debug.hh"

using namespace Theia::Vulkan;
using namespace std;

using Coeus::Severity;
using Theia::Log;

static Coeus::Log::Source _log("Vulkan"s);
static VKAPI_ATTR VkBool32 VKAPI_CALL callback(
	VkDebugUtilsMessageSeverityFlagBitsEXT,
	VkDebugUtilsMessageTypeFlagsEXT,
	VkDebugUtilsMessengerCallbackDataEXT const *,
	void *
);
static void defcb(Events::Debug const &);

Debug::Debug(
	optional<decltype(_cb)> const &cb
) noexcept :
	_info{
		.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
		.pNext = nullptr,
		.flags = 0,
		.messageSeverity =
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
		,
		.messageType =
			VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
		,
		.pfnUserCallback = callback,
		.pUserData = &_cb
	},
	_cb(cb ? *cb : defcb)
{}

auto Debug::Info() const noexcept -> decltype(_info) const & {
	return _info;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL callback(
	VkDebugUtilsMessageSeverityFlagBitsEXT sev,
	VkDebugUtilsMessageTypeFlagsEXT type,
	VkDebugUtilsMessengerCallbackDataEXT const *data,
	void *user
) {
	auto *f = reinterpret_cast<function<void(Events::Debug const &)> *>(user);

	if(!f || !user || !data)
		COEUS_LOG(Log, Severity::Critical) << "Repoirt callback with unusable data";
	else
		(*f)(Events::Debug(sev, type, data));

	return VK_FALSE;
}

static void defcb(Events::Debug const &ev) {
	COEUS_LOG(_log, ev.LogSeverity()) << ev.Message();
}
