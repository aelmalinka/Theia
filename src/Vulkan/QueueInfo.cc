/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "QueueInfo.hh"

using namespace Theia::Vulkan;
using namespace std;

QueueInfo::QueueInfo(
	decltype(_family) const &family,
	vector<float> &&prio
) :
	_family(family),
	_prio(move(prio)),
	_info{
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.pNext = nullptr,	// 2020-04-12 AMR TODO: VkDeviceQueueGlobalPriorityCreateInfoEXT?
		.flags = 0u,		// 2020-04-12 AMR TODO: Protected Memory?
		.queueFamilyIndex = _family->Index(),
		.queueCount = static_cast<uint32_t>(_prio.size()),
		.pQueuePriorities = _prio.data()
	}
{}

auto QueueInfo::Info() const -> decltype(_info) const & {
	return _info;
}

auto QueueInfo::Family() const -> decltype(_family) const & {
	return _family;
}
