/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DEBUG_INC
#	define THEIA_VULKAN_DEBUG_INC

#	include "Events.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			class Debug {
				private:
					VkDebugUtilsMessengerCreateInfoEXT _info;
					std::function<void(Events::Debug const &)> _cb;
				public:
					explicit Debug(
						std::optional<decltype(_cb)> const & = std::optional<decltype(_cb)>()
					) noexcept;
					auto Info() const noexcept -> decltype(_info) const &;
			};
		}
	}

#endif
