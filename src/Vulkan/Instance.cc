/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "Instance.hh"

using namespace Theia::Vulkan;
using namespace std;

Instance::Instance(string &&name, VersionType const &version) :
	Instance(move(ApplicationInfo(
		move(name),
		version
	).Add(Extension(
		"VK_KHR_surface"s, true
#	ifdef _WIN32
	)).Add(Extensions(
		"VK_KHR_win32_surface"s, true
#	else
	)).Add(Extension(
		"VK_KHR_xcb_surface"s, true
#	endif
#	ifdef DEBUG
	)).Add(Extension(
		VK_EXT_DEBUG_UTILS_EXTENSION_NAME, true
	)).Add(Extension(
		VK_EXT_DEBUG_REPORT_EXTENSION_NAME, true
	)).Add(Layer(
		// 2020-04-02 AMR TODO: khronos validation?
		"VK_LAYER_LUNARG_standard_validation"s
#	endif
	))))
{}

Instance::Instance(ApplicationInfo &&info) :
	_handle(VK_NULL_HANDLE),
	_info(move(info))
{
#	ifdef DEBUG
		// 2020-04-11 AMR NOTE: DEBUG_PRINTF requires !GPU_ASSISTED
		// 2020-04-11 AMR TODO: determine which we want
		auto features = vector<VkValidationFeatureEnableEXT>{
			VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT,
			VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT,
			VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT,
		};

		auto validate = VkValidationFeaturesEXT{
			.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT,
			.pNext = &_debug.Info(),
			.enabledValidationFeatureCount = static_cast<uint32_t>(features.size()),
			.pEnabledValidationFeatures = features.data(),
			.disabledValidationFeatureCount = 0,
			.pDisabledValidationFeatures = nullptr
		};
#	endif

	auto create = VkInstanceCreateInfo{
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
#		ifdef DEBUG
			.pNext = &validate,
#		else
			.pNext = nullptr,
#		endif
		.flags = 0,
		.pApplicationInfo = &_info.Handle(),
		.enabledLayerCount = static_cast<uint32_t>(_info.Layers().size()),
		.ppEnabledLayerNames = _info.Layers().data(),
		.enabledExtensionCount = static_cast<uint32_t>(_info.Extensions().size()),
		.ppEnabledExtensionNames = _info.Extensions().data(),
	};

#	ifdef DEBUG
	COEUS_LOG(Log, Coeus::Severity::Debug) << "Loading " << _info.Extensions().size() << " Extensions";
	for(auto &e : _info.Extensions()) {
		COEUS_LOG(Log, Coeus::Severity::Debug) << e;
	}
	COEUS_LOG(Log, Coeus::Severity::Debug) << "Loading " << _info.Layers().size() << " Layers";
	for(auto &e : _info.Layers()) {
		COEUS_LOG(Log, Coeus::Severity::Debug) << e;
	}
#	endif

	EVK_SUCCESS_OR_THROW(vkCreateInstance(&create, nullptr, &_handle), "Failed to create Instance");
}

Instance::~Instance() {
	vkDestroyInstance(_handle, nullptr);
}

auto Instance::Handle() const -> decltype(_handle) const & {
	return _handle;
}

auto Instance::Info() const -> decltype(_info) const & {
	return _info;
}
