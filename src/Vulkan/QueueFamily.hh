/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_QUEUEFAMILY_INC
#	define THEIA_VULKAN_QUEUEFAMILY_INC

#	include "Surface.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			class PhysicalDevice;

			class QueueFamily {
				private:
					std::shared_ptr<PhysicalDevice> _device;
					std::uint32_t _index;
					VkQueueFamilyProperties _prop;
				protected:
					QueueFamily(decltype(_device) const &, decltype(_index) &&, decltype(_prop) &&);
				public:
					QueueFamily(QueueFamily const &) = delete;
					QueueFamily(QueueFamily &&);
					auto getPhysicalDevice() const -> decltype(_device) const &;
					auto Index() const -> decltype(_index) const &;
					auto Properties() const -> decltype(_prop) const &;
					auto canPresent(std::shared_ptr<Surface> const &) const -> bool;
					static auto get(std::shared_ptr<PhysicalDevice> const &) -> std::vector<QueueFamily>;
			};
		}
	}

#	include "PhysicalDevice.hh"

#endif
