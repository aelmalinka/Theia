/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_PHYSICALDEVICE_INC
#	define THEIA_VULKAN_PHYSICALDEVICE_INC

#	include <memory>
#	include "QueueFamily.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			class PhysicalDevice :
				public std::enable_shared_from_this<PhysicalDevice>
			{
				private:
					std::shared_ptr<Instance> _instance;
					VkPhysicalDevice _handle;
					VkPhysicalDeviceProperties _prop;
					bool _has_prop;
					VkPhysicalDeviceFeatures _features;
					bool _has_features;
					VkPhysicalDeviceMemoryProperties _mem_prop;
					bool _has_mem_prop;
					std::vector<QueueFamily> _families;
				protected:
					PhysicalDevice(decltype(_instance) const &, decltype(_handle) &&);
				public:
					PhysicalDevice(PhysicalDevice const &) = delete;
					PhysicalDevice(PhysicalDevice &&);
					auto Handle() const -> decltype(_handle) const &;
					auto getInstance() const -> decltype(_instance) const &;
					auto Properties() -> decltype(_prop) const &;
					auto Features() -> decltype(_features) const &;
					auto MemoryProperties() -> decltype(_mem_prop) const &;
					auto QueueFamilies() -> decltype(_families) const &;
					// 2020-04-07 AMR TODO: there are probably better systems for this
					static auto select(
						std::shared_ptr<Instance> const &,
						std::function<int(std::shared_ptr<PhysicalDevice> const &)> const &
					) -> std::shared_ptr<PhysicalDevice>;
			};
		}
	}

#	include "QueueFamily.hh"

#endif
