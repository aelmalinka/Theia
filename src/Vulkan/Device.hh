/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DEVICE_INC
#	define THEIA_VULKAN_DEVICE_INC

#	include "PhysicalDevice.hh"
#	include "QueueInfo.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			class Device {
				private:
					std::shared_ptr<PhysicalDevice> _phys;
					VkDevice _handle;
				public:
					// 2020-04-08 AMR TODO: this could probably be better
					Device(decltype(_phys) const &, std::vector<QueueInfo> const &, std::vector<char const *> const & = {});
					Device(Device const &) = delete;
					~Device();
					auto Handle() const -> decltype(_handle) const &;
					auto getPhysical() const -> decltype(_phys) const &;
					auto wait() -> void;
			};
		}
	}

#endif
