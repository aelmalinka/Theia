/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "ApplicationInfo.hh"
#include "detail/call.hh"

using namespace Theia::Vulkan;
using namespace std;

ApplicationInfo::ApplicationInfo(
	string &&name,
	uint32_t const version
) :
	_handle(),
	_name(move(name))
{
	_handle.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	_handle.pApplicationName = _name.c_str();
	_handle.applicationVersion = version;
	_handle.pEngineName = "Theia";
	_handle.engineVersion = Version;

	auto vkEnumerateInstanceVersion = detail::call<PFN_vkEnumerateInstanceVersion>("vkEnumerateInstanceVersion"s);

	if(!vkEnumerateInstanceVersion)
		_handle.apiVersion = VK_API_VERSION_1_0;
	else
		EVK_SUCCESS_OR_THROW(
			vkEnumerateInstanceVersion(&_handle.apiVersion),
			"Failed to get available instance version"s
		);
}

auto ApplicationInfo::Handle() const -> decltype(_handle) const & {
	return _handle;
}

auto ApplicationInfo::Extensions() const -> decltype(_extensions) const & {
	return _extensions;
}

auto ApplicationInfo::Layers() const -> decltype(_layers) const & {
	return _layers;
}

auto ApplicationInfo::Add(Extension &&ext) -> decltype(*this) {
	if(ext.isFound())
		_extensions.emplace_back(_ext.emplace_back(move(ext)).Name());

	return *this;
}

auto ApplicationInfo::Add(Layer &&lay) -> decltype(*this) {
	if(lay.isFound())
		_layers.emplace_back(_lay.emplace_back(move(lay)).Name());

	return *this;
}
