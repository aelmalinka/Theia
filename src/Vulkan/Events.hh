/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_INC
#	define THEIA_VULKAN_INC

#	include "../Event.hh"
#	include "Exception.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			namespace Events
			{
				class Debug :
					public Event
				{
					public:
						static id_type const Id;
					private:
						VkDebugUtilsMessageSeverityFlagBitsEXT _severity;
						VkDebugUtilsMessageTypeFlagsEXT _type;
						VkDebugUtilsMessengerCallbackDataEXT const *_data;
					public:
						Debug(
							decltype(_severity) const,
							decltype(_type) const,
							decltype(_data)
						);
						auto Severity() const -> decltype(_severity) const &;
						auto Type() const -> decltype(_type) const &;
						auto Data() const -> decltype(_data);
						auto LogSeverity() const -> Coeus::Severity;
						auto TypeName() const -> std::string;
						auto Message() const -> std::string;
				};
			}
		}
	}

#endif
