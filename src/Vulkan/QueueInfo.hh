/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_QUEUEINFO_INC
#	define THEIA_VULKAN_QUEUEINFO_INC

#	include "PhysicalDevice.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			class QueueInfo {
				private:
					decltype(std::declval<PhysicalDevice>().QueueFamilies().begin()) _family;
					std::vector<float> _prio;
					VkDeviceQueueCreateInfo _info;
				public:
					explicit QueueInfo(
						decltype(_family) const &,
						decltype(_prio) && = decltype(_prio){ 1.0f, }
					);
					auto Info() const -> decltype(_info) const &;
					auto Family() const -> decltype(_family) const &;
					template<typename C>
					static auto Convert(C const &) -> std::vector<VkDeviceQueueCreateInfo>;
			};
		}
	}

#	include "QueueInfo.impl.hh"

#endif
